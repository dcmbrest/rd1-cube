/*
   Отображение текущего состояния реле внизу экрана.
*/
byte INITIAL_TEXT_SIZE = 2;
byte CURRENT_TEXT_SIZE = INITIAL_TEXT_SIZE;
byte INITIAL_BLOCK_HIGH = 64;
byte CURRENT_BLOCK_HIGH = INITIAL_BLOCK_HIGH;
bool idlingState = false;

void printRelaysStats() {
  if (!idlingState) return;

  tft.setTextSize(CURRENT_TEXT_SIZE);
//  tft.fill(255, 100, 0);
  byte h = tft.height() - CURRENT_BLOCK_HIGH * 2;
//  tft.rect(0, h, 160, CURRENT_BLOCK_HIGH);
  tft.setCursor(0, h + 5);
  tft.stroke(Black);
  if (idlingState) {
    unsigned long currentMillis;
    if (on == HIGH) {
      tft.fill(40, 200, 0);
      //byte h = tft.height() - CURRENT_BLOCK_HIGH * 2;
      tft.rect(0, h, 160, CURRENT_BLOCK_HIGH);
      tft.print(" ON ");
      currentMillis = millis() - startTime;
    }
    else {
      tft.fill(0, 0, 250);
      //byte h = tft.height() - CURRENT_BLOCK_HIGH * 2;
      tft.rect(0, h, 160, CURRENT_BLOCK_HIGH);
      tft.print(" OFF ");
      currentMillis = konecTime - startTime;

    }

    unsigned long seconds = currentMillis / 1000;
    byte minutes = seconds / 60;
    byte hours = minutes / 60;
    currentMillis %= 1000;
    seconds %= 60;
    minutes %= 60;
    hours %= 24;
    tft.print(hours);
    tft.print(":");
    tft.print(minutes);
    tft.print(":");
    tft.print(seconds);
    tft.println();
  }
  tft.print(" r1:");
  tft.print(tankFirstRelay);
  tft.print(" r2:");
  tft.print(tankSecondRelay);
  tft.print(" "); tft.print(currentPower); tft.print("% ");
  if (idlingState) {
    tft.println();
  }
  tft.print(" r3:");
  tft.print(waterPump);
  tft.print(" r4:");
  tft.print(waterValve);
  tft.setCursor(0, 0);
  tft.setTextSize(2);
}

/*
  Отображение текущей температуры внизу экрана.
*/
void printTempStats() {
  if (!idlingState) return;

  tft.setTextSize(CURRENT_TEXT_SIZE);
  tft.fill(255, 255, 0);
  byte h = tft.height() - CURRENT_BLOCK_HIGH;
  tft.rect(0, h, 160, CURRENT_BLOCK_HIGH);
  tft.setCursor(0, h + 5);
  tft.stroke(Black);
  tft.print(" kub:");
  tft.print(getTempTank());
  if (idlingState) {
    tft.println();
  }
  tft.print(" col:");
  tft.print(getTempCol());
  if (idlingState) {
    tft.println();
  }
  tft.print(" wat:");
  tft.print(getTempWater());
  tft.setCursor(0, 0);
  tft.setTextSize(2);
}
