/*
   Считываем данные температуры
*/

#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 3
#define TEMP_SENSOR_INDEX_TANK 0
#define TEMP_SENSOR_INDEX_COL 1
#define TEMP_SENSOR_INDEX_WATER 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

// получение темепратуры бака
byte getTempTank() {
  return sensors.getTempCByIndex(TEMP_SENSOR_INDEX_TANK);
}

// получение темепратуры колонны
byte getTempCol() {
  return sensors.getTempCByIndex(TEMP_SENSOR_INDEX_COL);
}

// получение темепратуры воды
byte getTempWater() {
  return sensors.getTempCByIndex(TEMP_SENSOR_INDEX_WATER);
}
