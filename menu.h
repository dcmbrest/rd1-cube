/**
   Создание пунктов меню.
*/

result showEvent(eventMask e, navNode& nav, prompt& item) {

  return proceed;
}

unsigned long startTime;
unsigned long konecTime;

void startTimer () {
  startTime = millis();
}

byte on = LOW;
byte waterForceOn = LOW;
TOGGLE(on, onOffMenu, "Start: ", doNothing, noEvent, wrapStyle
       , VALUE("ON", HIGH, startTimer, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );

TOGGLE(waterForceOn, onOffForceWaterMenu, "wat_test: ", doNothing, noEvent, wrapStyle
       , VALUE("ON", HIGH, startTimer, noEvent)
       , VALUE("OFF", LOW, doNothing, noEvent)
      );

MENU(gidroSub, "Gidro", showEvent, anyEvent, noStyle
     , FIELD(waterIdeal, "start", "C", 0, 90, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(waterStop, "stop", "C", 0, 90, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(waterGist, "gist", "C", 0, 10, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , SUBMENU(onOffForceWaterMenu)
     , EXIT("<-Back")
    );

MENU(tankSub, "Tank", showEvent, anyEvent, noStyle
     , FIELD(tankMax, "max", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(tankGist, "gist", "C", 0, 10, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(konec, "konec", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(zaderzhka, "zaderzhka", "s", 0, 500, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , EXIT("<-Back")
    );

MENU(colbSub1, "Colb range 1", showEvent, anyEvent, noStyle
     , FIELD(colb1Min, "min", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(colb1Max, "max", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(power1, "power", "%", 0, 100, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , EXIT("<-Back")
    );

MENU(colbSub2, "Colb range 2", showEvent, anyEvent, noStyle
     , FIELD(colb2Min, "min", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(colb2Max, "max", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(power2, "power", "%", 0, 100, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , EXIT("<-Back")
    );

MENU(colbSub3, "Colb range 3", showEvent, anyEvent, noStyle
     , FIELD(colb3Min, "min", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(colb3Max, "max", "C", 0, 110, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , FIELD(power3, "power", "%", 0, 100, 1, 0, Menu::doNothing, Menu::noEvent, Menu::noStyle)
     , EXIT("<-Back")
    );


MENU(colbSub, "Colb", showEvent, anyEvent, noStyle
     , SUBMENU(colbSub1)
     , SUBMENU(colbSub2)
     , SUBMENU(colbSub3)
     , EXIT("<-Back")
    );

MENU(mainMenu, "RD1.CUBE menu", Menu::doNothing, Menu::noEvent, Menu::wrapStyle
     , SUBMENU(tankSub)
     , SUBMENU(colbSub)
     , SUBMENU(gidroSub)
     , SUBMENU(onOffMenu)
    );
