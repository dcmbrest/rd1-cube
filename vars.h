/**
   Инициализация всех переменных
*/



const colorDef<uint16_t> colors[6] MEMMODE = {
  {{ST7735_BLACK, ST7735_BLACK}, {ST7735_BLACK, ST7735_BLUE, ST7735_BLUE}}, //bgColor
  {{ST7735_BLACK, ST7735_BLACK}, {ST7735_WHITE, ST7735_WHITE, ST7735_WHITE}}, //fgColor
  {{ST7735_WHITE, ST7735_BLACK}, {ST7735_YELLOW, ST7735_YELLOW, ST7735_RED}}, //valColor
  {{ST7735_WHITE, ST7735_BLACK}, {ST7735_WHITE, ST7735_YELLOW, ST7735_YELLOW}}, //unitColor
  {{ST7735_WHITE, ST7735_BLACK}, {ST7735_BLACK, ST7735_BLUE, ST7735_WHITE}}, //cursorColor
  {{ST7735_WHITE, ST7735_YELLOW}, {ST7735_WHITE, ST7735_BLACK, ST7735_BLACK}}, //titleColor
};


byte tankFirstRelay = LOW;
byte tankSecondRelay = LOW;
byte waterPump = LOW;
byte waterValve = LOW;
byte currentPower = 0;

byte waterIdeal = EEPROM.read(0);
byte waterGist = EEPROM.read(1);
byte tankMax = EEPROM.read(2);
byte tankGist = EEPROM.read(3);
byte colb1Min = EEPROM.read(4);
byte colb2Min = EEPROM.read(5);
byte colb3Min = EEPROM.read(6);
byte colb1Max = EEPROM.read(7);
byte colb2Max = EEPROM.read(8);
byte colb3Max = EEPROM.read(9);
byte power1 = EEPROM.read(10);
byte power2 = EEPROM.read(11);
byte power3 = EEPROM.read(12);
byte konec = EEPROM.read(13);
unsigned int zaderzhka = EEPROM.read(14);
byte waterStop = EEPROM.read(15);

void updateVars() {
  EEPROM.update(0, waterIdeal);
  EEPROM.update(1, waterGist);
  EEPROM.update(2, tankMax);
  EEPROM.update(3, tankGist);
  EEPROM.update(4, colb1Min);
  EEPROM.update(5, colb2Min);
  EEPROM.update(6, colb3Min);
  EEPROM.update(7, colb1Max);
  EEPROM.update(8, colb2Max);
  EEPROM.update(9, colb3Max);
  EEPROM.update(10, power1);
  EEPROM.update(11, power2);
  EEPROM.update(12, power3);
  EEPROM.update(13, konec);
  EEPROM.update(14, zaderzhka);
  EEPROM.update(15, waterStop);
}

void initVars() {
  if (waterIdeal == 255) {
    waterIdeal = 0;
    EEPROM.update(0, 0);
  }
  if (waterGist == 255) {
    waterGist = 0;
    EEPROM.update(1, 0);
  }

  if (tankMax == 255) {
    tankMax = 0;
    EEPROM.update(2, 0);
  }

  if (tankGist == 255) {
    tankGist = 0;
    EEPROM.update(3, 0);
  }

  if (colb1Min == 255) {
    colb1Min = 0;
    EEPROM.update(4, 0);
  }

  if (colb2Min == 255) {
    colb2Min = 0;
    EEPROM.update(5, 0);
  }

  if (colb3Min == 255) {
    colb3Min = 0;
    EEPROM.update(6, 0);
  }

  if (colb1Max == 255) {
    colb1Max = 0;
    EEPROM.update(7, 0);
  }

  if (colb2Max == 255) {
    colb2Max = 0;
    EEPROM.update(8, 0);
  }

  if (colb3Max == 255) {
    colb3Max = 0;
    EEPROM.update(9, 0);
  }

  if (power1 == 255) {
    power1 = 0;
    EEPROM.update(10, 0);
  }

  if (power2 == 255) {
    power2 = 0;
    EEPROM.update(11, 0);
  }

  if (power3 == 255) {
    power3 = 0;
    EEPROM.update(12, 0);
  }

  if (konec == 255) {
    konec = 0;
    EEPROM.update(13, 0);
  }

  
  if (zaderzhka == 255) {
    zaderzhka = 0;
    EEPROM.update(14, 0);
  }

  if (waterStop == 255) {
    waterStop = 0;
    EEPROM.update(15, 0);
  }
}
