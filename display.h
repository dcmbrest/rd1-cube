/**
   Инициализация дисплея.
   Разрешение, шрифты, цвета и т.п.
*/

#include <EEPROM.h>
#include <TFT.h> // Hardware-specific library
#include <SPI.h>
#include <menu.h>
#include <menuIO/tftOut.h>

//display size
#define TFT_Width 160
#define TFT_Height 128
//font size plus margins
#define fontX 16
#define fontY 18

#define CS   10
#define DC   9
#define RST  8

#define Black RGB565(0,0,0)
#define Red  RGB565(255,0,0)
#define Green RGB565(0,255,0)
#define Blue RGB565(0,0,255)
#define Gray RGB565(128,128,128)
#define LighterRed RGB565(255,150,150)
#define LighterGreen RGB565(150,255,150)
#define LighterBlue RGB565(150,150,255)
#define DarkerRed RGB565(150,0,0)
#define DarkerGreen RGB565(0,150,0)
#define DarkerBlue RGB565(0,0,150)
#define Cyan RGB565(0,255,255)
#define Magenta RGB565(255,0,255)
#define Yellow RGB565(255,255,0)
#define White RGB565(255,255,255)
using namespace Menu;

#define MAX_DEPTH 5

const char* constMEM hexDigit MEMMODE = "0123456789ABCDEF";
const char* constMEM hexNr[] MEMMODE = {"0", "x", hexDigit, hexDigit};
char buf1[] = "0x11"; //<-- menu will edit this text


//// DISPLAY INIT /////////////////////////////////////////////////////////////////////////
TFT tft(CS, DC, RST);
//////////////////////////////////////////////////////////////////////////////////////////
