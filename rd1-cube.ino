/*
   Главный файл проекта, в нем подключаются другие файлы и библиотеки.
   Здесь ничего рtедкатировать не надо, все изменения делаются в других файлах.
*/
#include "display.h"
#include "keypad.h"
#include "vars.h"
#include "temp_sensors.h"
#include "menu.h"
#include "stats.h"
#include "update_relays.h"

//define outputs controller
idx_t ucg_tops[MAX_DEPTH];
//PANELS(ucgPanels, {0, 0, TFT_Width / fontX, TFT_Height / fontY});
PANELS(ucgPanels, {0, 0, TFT_Width / 2, TFT_Height / 2});
TFTOut tftOut(tft, colors, ucg_tops, ucgPanels, fontX, fontY);

menuOut* const outputs[] MEMMODE = {&tftOut}; //list of output devices
outputsList out(outputs, 1); //outputs list controller

// keypadIn kpad(customKeypad);
//define navigation root and aux objects
navNode nav_cursors[MAX_DEPTH];//aux objects to control each level of navigation
navRoot nav(mainMenu, nav_cursors, MAX_DEPTH, in, out);
byte TIMEOUT = 5;
int stats = 0;
//unsigned long prevStartTime = 0;
void update() {
 
  if (!idlingState) {
    if (!on) {
      if (waterForceOn) {
        nav.timeOut = 10000;
        startWater();
      }
      else {
        nav.timeOut = TIMEOUT;
        stopWater();
      }
    }
    return;
  } 

  stats++;
  if (stats != 10000) {
    return;
  }
  stats = 0;

  sensors.requestTemperatures();
  updateVars();
  updateRelays();
  printTempStats();
  printRelaysStats();
}

#include "idle.h"
void setup() {
  initVars();
  encoder.begin();
  nav.timeOut = TIMEOUT;
  nav.idleTask = idleScreen; //point a function to be used when menu is suspended
  tft.begin();
  tft.setTextSize(2);
  tft.background(0, 0, 0); // clear the screen with black
  tft.setRotation(3);
  tft.setTextWrap(false);
  sensors.begin();  // start 1-wire sensors

  pinMode(RELAY_PIN_TANK_FIRST, OUTPUT);
  pinMode(RELAY_PIN_WATER_VALVE, OUTPUT);
  pinMode(RELAY_PIN_WATER_PUMP, OUTPUT);

  pinMode(encA, INPUT);
  pinMode(encB, INPUT);
}

void loop() {
  nav.poll();
  update();
}
