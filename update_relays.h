/**
   Логика работы аппарата - что и при каких условиях включать и выключать.
*/

// TODO надо прописать реальные пины реле
#define RELAY_PIN_TANK_FIRST 4  // главное реле бака
#define RELAY_PIN_TANK_SECOND 5 // твердотельное реле бака
#define RELAY_PIN_WATER_VALVE 2 // реле гидро клапана
#define RELAY_PIN_WATER_PUMP 7  // реле насоса

byte waterTemp;
// выключить воду
void stopWater()
{
  if (waterPump == LOW && waterValve == LOW)
  {
    return;
  }

  waterPump = LOW;
  waterValve = LOW;
  digitalWrite(RELAY_PIN_WATER_PUMP, waterPump);
  delay(1000);
  digitalWrite(RELAY_PIN_WATER_VALVE, waterValve);
}

// включить воду
void startWater()
{
  if (waterPump == HIGH && waterValve == HIGH)
  {
    return;
  }
  waterPump = HIGH;
  waterValve = HIGH;
  digitalWrite(RELAY_PIN_WATER_VALVE, waterValve);
  delay(1000);
  digitalWrite(RELAY_PIN_WATER_PUMP, waterPump);
}

// вкл/выкл бак
void tankToggle(byte val)
{
  tankFirstRelay = val;
  digitalWrite(RELAY_PIN_TANK_FIRST, tankFirstRelay);
}

// изменение шим
void updateTankSecondRelay(byte power)
{
  currentPower = power;
  //  tankSecondRelay = HIGH;
  //  analogWrite(RELAY_PIN_TANK_SECOND, 255 * power / 100);
  if (power < millis() / 1000 % 10 * 10)
  {
    // turn off
    tankSecondRelay = LOW;
  }
  else
  {
    // turn on
    tankSecondRelay = HIGH;
  }
  digitalWrite(RELAY_PIN_TANK_SECOND, tankSecondRelay);
}

// логика включения главного реле нагревателя
void updateTankFirstRelay()
{
  if (waterTemp >= waterStop)
  {
    return;
  }
  byte tankTemp = getTempTank();
  if (tankTemp > konec)
  {
    konecTime = millis();
    on = LOW;
    return;
  }

  if (tankTemp < tankMax)
  {
    tankToggle(HIGH);
  }
  else
  {
    tankToggle(LOW);
  }
}

// логика работы колонны
void updateCol()
{
  if (waterTemp >= waterStop)
  {
    return;
  }
  byte colTemp = getTempCol();
  if (colTemp > colb1Min && colTemp < colb1Max)
  {
    updateTankSecondRelay(power1);
  }
  if (colTemp > colb2Min && colTemp < colb2Max)
  {
    updateTankSecondRelay(power2);
  }
  if (colTemp > colb3Min && colTemp < colb3Max)
  {
    updateTankSecondRelay(power3);
  }
}

// логика работы охлаждающей части
void updateWater()
{
  waterTemp = getTempWater();

  if (waterTemp >= waterStop)
  {
    tankToggle(LOW);
    tankSecondRelay = LOW;
    analogWrite(RELAY_PIN_TANK_SECOND, 0);
  }

  if (waterTemp > (waterIdeal - waterGist) && waterTemp < (waterIdeal + waterGist))
  {
    // do nothing
    return;
  }

  else if (waterTemp < (waterIdeal - waterGist))
  {
    stopWater();
    return;
  }

  else if (waterTemp > (waterIdeal + waterGist))
  {
    startWater();
  }
}

// выключить всю систему
void turnOff()
{
  stopWater();
  tankToggle(LOW);

  if (startTime > 0)
  {
    if ((millis() - konecTime) / 1000 > zaderzhka)
    {
      tankSecondRelay = LOW;
      analogWrite(RELAY_PIN_TANK_SECOND, 0);
      currentPower = 0;
    }
  }
}

// это функция вызывается каждую секунду
void updateRelays()
{
  if (on != HIGH)
  {
    turnOff();
    return;
  }

  updateTankFirstRelay();
  updateCol();
  updateWater();
}
