/**
   Файл отвечает за отображение экрана состояния
*/
//when menu is suspended
//this function is called when entering or leaving suspended state
// with idleStart and idleend cases
//and at least once in between them (idling case)
//it might also be called for every pool (when in suspended state, idling case)
//for output devices that require refresh (repeated draw, idling case)
result idleScreen(menuOut &o, idleEvent e) {
// o.clear();
  switch (e) {
    case idleStart:
      CURRENT_TEXT_SIZE = 2;
      CURRENT_BLOCK_HIGH = 64;
      idlingState = true;
      break;
    case idling:
      break;
    case idleEnd:
//      CURRENT_TEXT_SIZE = INITIAL_TEXT_SIZE;
//      CURRENT_BLOCK_HIGH = INITIAL_BLOCK_HIGH;
      idlingState = false;
      //on = LOW;
      break;
  }
  return proceed;
}
